# Portfolio

### *A responsive Portfolio site generator for GitLab Pages. Powered by Mustache templates engine, GitLab CI and hmlt5up.*

## Quick Start 🚀

1. Fork this repository

2. Modify **data.json** with your information.

3. Push your changes to gitlab. 

4. Wait for the build on your pipeline and Done! 

Now your portfolio is public on https://{username}.gitlab.io/portfolio/ 
The default example is here [https://sergioignacio.gitlab.io/Portfolio/](https://sergioignacio.gitlab.io/Portfolio/)

## Work locally 

To work locally do a **npm install** and **npm run build** to generate the html files on the **public** folder. 

## Customize your Portfolio

To add your style just modify the main.css file in the public folder or the index.mustache template file to change the layout.


## Contribution

Just fork this repo ( [https://gitlab.com/sergioignacio/Portfolio/](https://gitlab.com/sergioignacio/Portfolio/) ) and make a merge request, or drop a mail at [sergioignacio.ov@gmail.com](mailto:sergioignacio.ov@gmail.com)


## License©️

Default theme designed by [html5up](https://html5up.net/).

This Portfolio is licensed under the MIT Open Source license. For more information, see the LICENSE file in this repository.